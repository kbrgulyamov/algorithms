// ----------------------------------------------------------

// Data Structures
// Array
// Obj
// Set
// Map


// ----------------------------------------------------------

class Node {
  constructor(data, next = null) {
    this.data = data
    this.next = next
  }
}
class LinkedList {
  constructor() {
    this.head = null
    this.tail = null
  }



  append(data) {
    const node = new Node(data)

    if (this.tail) {
      this.tail.next = node
    }

    if (!this.head) {
      this.head = node
    }

    this.tail = node
  }

  prepend(data) {
    const node = new Node(data, this.head)

    this.head = node

    if (!this.tail) {
      this.tail = node
    }
  }

  toArray() {
    const output = []
    let current = this.head
    while (current) {
      current = current.next
    }

    return output
  }

  find(data) {
    if (!this.head) {
      return
    }

    let current = this.head
    while (current) {
      if (current.data === data) {
        return current
      }
      current = current.next
    }
  }
}

const list = new LinkedList();
list.prepend('My')
list.append('name')
list.append('Is')
list.append('Akbar')
list.append('Gulyamov')
list.toArray()
console.log(list)
